<?php
use Symfony\Component\HttpFoundation\Request;

class Business
{
    /** @var \Silex\Application */
    private $app;

    /**
     * Business constructor.
     * @param \Silex\Application $app
     */
    public function __construct(\Silex\Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return bool
     */
    public function validatePutParameters(\Symfony\Component\HttpFoundation\Request $request)
    {
        $name = $request->get('name', null);
        $description = $request->get('description', null);
        $price = $request->get('price', null);

        foreach ([$name, $description, $price] as $item) {
            if (null !== $item) {
                return false;
            }
        }

        if (!is_float($price)) {
            return false;
        }

        return true;
    }

    /**
     * @param $id
     * @param \Doctrine\DBAL\Driver\Connection $conn
     * @return bool
     */
    public function itemExists($id, $conn)
    {
        $countSql = 'SELECT COUNT(*) AS cant FROM items WHERE id = :id';
        $stmtCount = $conn->prepare($countSql);
        $stmtCount->bindValue('id', $id);
        $cant = $stmtCount->fetchAll(PDO::FETCH_ASSOC);

        return $cant > 0;
    }

    /**
     * @param string $name
     * @param string $description
     * @param string $price
     * @return int
     */
    public function insertItem($name, $description, $price)
    {
        $sql = 'INSERT INTO items (name, description, price) VALUES (:name, :description, :price)';

        /** @var \Doctrine\DBAL\Driver\Connection $conn */
        $conn = $this->app['db'];
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('name', $name);
        $stmt->bindValue('description', $description);
        $stmt->bindValue('price', $price);

        $stmt->execute();

        return $conn->lastInsertId();
    }

    /**
     * @param string $name
     * @param string $description
     * @param string $price
     * @param int $id
     */
    public function updateItem($name, $description, $price, $id)
    {
        $sql = 'UPDATE items SET name = :name, description = :description, price = :price WHERE id = :id';

        /** @var \Doctrine\DBAL\Driver\Connection $conn */
        $conn = $this->app['db'];
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('name', $name);
        $stmt->bindValue('description', $description);
        $stmt->bindValue('price', $price);
        $stmt->bindValue('id', $id);

        $stmt->execute();
    }

    /**
     * @return array
     */
    public function listItems()
    {
        /** @var \Doctrine\DBAL\Driver\Connection $conn */
        $conn = $this->app['db'];

        $sql = 'SELECT * FROM items';
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function validatePostParameters(Request $request)
    {
        $params = ['customer_id', 'item_id'];
        /** @var \Doctrine\DBAL\Driver\Connection $conn */
        $conn = $this->app['db'];

        foreach ($params as $index => $param) {
            $value = $request->get($param, null);


            if (null === $value || (intval($value)) && (!is_int(intval($value)))) {
                return false;
            }
        }

        if (!$this->itemExists($request->get('item_id'), $conn)) {
            return false;
        }

        return true;
    }

    /**
     * @param $customerId
     * @param $itemId
     * @return int
     */
    public function insertItemIntoCart($customerId, $itemId)
    {
        $sql = 'INSERT INTO cart (customer_id, item_id) VALUES (:customer_id, :item_id)';

        /** @var \Doctrine\DBAL\Driver\Connection $conn */
        $conn = $this->app['db'];

        $stmt = $conn->prepare($sql);
        $stmt->bindValue('customer_id', $customerId);
        $stmt->bindValue('item_id', $itemId);
        $stmt->execute();

        return $conn->lastInsertId();
    }
}