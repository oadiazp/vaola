<?php
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

require_once __DIR__.'/../vendor/autoload.php';

date_default_timezone_set('Europe/Berlin');

$app = new Silex\Application();

$app['debug'] = true;

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'   => 'pdo_sqlite',
        'path'     => __DIR__.'/../vaola.sqlite',
    ),
));

$app['business'] = function($app) {
    require_once __DIR__ . '/business.php';
    return new Business($app);
};

$app->put('/items/save', function(Request $request, \Silex\Application $app) {
    if ('PUT' !== $request->getMethod()) {
        return new Response('Method not allowed');
    }

    $id = $request->get('id', null);
    $name = $request->get('name', null);
    $description = $request->get('description', null);
    $price = $request->get('price', null);

    if ($app['business']->validatePutParameters($request)) {
        return new Response('Bad request', 400);
    }

    if (null === $id) {
        try {
            $id = $app['business']->insertItem($name, $description, $price);
            return new Response($id, 200);
        }
        catch (\Exception $e) {
            return new Response($e->getMessage(), 500);
        }
    } else {
        try {
            if ($app['business']->itemExists($id, $app['conn']))
            $app['business']->updateItem($id, $name, $description, $price);
            return new Response($id, 200);
        }
        catch (\Exception $e) {
            return new Response($e->getMessage(), 500);
        }
    }
});

$app->get('/items/list', function(Request $request, \Silex\Application $app) {
    if ('GET' !== $request->getMethod()) {
        return new Response('Method not allowed');
    }

    return new Response(json_encode($app['business']->listItems()), 200);
});

$app->post('/cart/add', function(Request $request, Application $app) {
    if ('POST' !== $request->getMethod()) {
        return new Response('Method not allowed');
    }

    try {
        if ($app['business']->validatePostParameters($request)) {
            $id = $app['business']->insertItemIntoCart(
                $request->get('customer_id'),
                $request->get('item_id')
            );
            return new Response($id, 201);
        } else {
            return new Response('Bad request', 400);
        }
    }
    catch (Exception $e) {
        return new Response($e->getMessage(), 500);
    }
});

$app->run();