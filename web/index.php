<?php
/**
 * Created by PhpStorm.
 * User: omar
 * Date: 03/03/16
 * Time: 14:41
 */

$filename = __DIR__.preg_replace('#(\?.*)$#', '', $_SERVER['REQUEST_URI']);
if (php_sapi_name() === 'cli-server' && is_file($filename)) {
    return false;
}

require __DIR__.'/app.php';
