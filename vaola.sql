CREATE TABLE cart
(
    customer_id INTEGER,
    item_id INTEGER NOT NULL
);
CREATE TABLE items
(
    name TEXT,
    description TEXT,
    price REAL,
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL
);
CREATE UNIQUE INDEX items_id_uindex ON items (id);